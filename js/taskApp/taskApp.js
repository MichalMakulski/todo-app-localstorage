(function(public, htmlElts){ 

  public.taskApp = public.taskApp || {};
  
  // main task array:
  var taskList;

  // fallback to document.cookie
  function saveData(key, value){
    if(window.localStorage){
      localStorage.setItem(key, value);
    }else{
      document.cookie = key + '=' + value;
    }
  }
  
  function getData(key){
    if(window.localStorage){
      taskList = localStorage[key] ? JSON.parse(localStorage[key]) : [];
    }else{
        var cookies = document.cookie.replace(/\s/g, '').split(';');
        var o = {};
        function assignToObj(cookie){
          var c = cookie.replace(/=/, ';').split(';');
          o[c[0]] = c[1];
        }
        cookies.forEach(assignToObj);
        document.cookies = o;
        taskList =  document.cookies[key] ? JSON.parse(document.cookies[key]) : [];
    }
  }

  // editTask combines 3 scenarios - clearing, editing and deleting task:
  function editTask(ev){
    ev.preventDefault();
    var target = ev.target;
    if(target.classList.contains('task-item')){
      var completed = taskList[target.dataset.taskid].completed;
      target.classList.toggle('task-done');
      taskList[target.dataset.taskid].completed = !completed;
      saveData('tasks', JSON.stringify(taskList));
    }else if(target.classList.contains('delete-btn')) {
      if(confirm('Are you sure you want to delete this task?')){
        var taskEl = target.parentElement.dataset.taskid;
        taskList.splice(parseInt(taskEl), 1);
        saveData('tasks', JSON.stringify(taskList));
        taskApp.view.render(taskList, htmlElts.list, taskApp.view.taskTemplate);
        
        taskApp.filter.updateCategories(taskList);
        taskApp.view.render(taskApp.allCategories, htmlElts.categoryList, taskApp.view.categoryTemplate);
      }
    }else if(target.classList.contains('edit-btn')) {
      var taskEl = target.parentElement.dataset.taskid;    
      
      // populate form inputs with task data:
      htmlElts.name.value = taskList[taskEl].name;
      htmlElts.desc.value = taskList[taskEl].description;
      htmlElts.deadline.value = taskList[taskEl].deadline;
      htmlElts.categories.value = taskList[taskEl].categories.join(',');
      
      taskApp.filter.updateCategories(taskList);
      taskApp.view.render(taskApp.allCategories, htmlElts.categoryList, taskApp.view.categoryTemplate);
      
      // modify form submit button:
      htmlElts.btn.value = 'SAVE TASK';
      htmlElts.btn.style.backgroundColor = '#BA68C8';
      
      // modify form properties:
      htmlElts.form.mode = 'update';
      htmlElts.form.editedTask = taskList[taskEl];
    } 
  }
  
  // adds or updates the task (depends on form.mode value):
  function addTask(ev) {
    ev.preventDefault(); 
    var taskName = htmlElts.name.value;
    var taskDesc = htmlElts.desc.value;
    var deadlineDate = htmlElts.deadline.value;
    var taskCategories = htmlElts.categories.value.replace(/\s/g, '').split(',');
    if (!taskName) {
      alert('Name your task...');
    }else {
      if(htmlElts.form.mode === 'update'){
        htmlElts.form.editedTask.name = taskName;
        htmlElts.form.editedTask.description = taskDesc;
        htmlElts.form.editedTask.deadline = deadlineDate;
        htmlElts.form.editedTask.categories = taskCategories;
        htmlElts.form.mode = 'create';
        htmlElts.btn.value = 'ADD TASK';
        htmlElts.btn.style.backgroundColor = '';
      }else {
        var newTask = {
          name: taskName,
          description: taskDesc,
          deadline: deadlineDate,
          categories: taskCategories
        }
        taskList.push(newTask);
      }  
    }  
    saveData('tasks', JSON.stringify(taskList));
    taskApp.view.render(taskList, htmlElts.list, taskApp.view.taskTemplate);
    
    taskApp.filter.updateCategories(taskList);
    taskApp.view.render(taskApp.allCategories, htmlElts.categoryList, taskApp.view.categoryTemplate);
  }
  
  function bindEvents(){
    htmlElts.form.addEventListener('submit', addTask, false);
    htmlElts.list.addEventListener('click', editTask, false);
    htmlElts.categoryList.addEventListener('click', function(ev){
      taskApp.filter.filterTasks(ev, taskList) 
    }, false);
  }

  taskApp.init = function(){
    taskApp.htmlTaskList = htmlElts.list;
    bindEvents();
    getData('tasks');
    taskApp.view.render(taskList, htmlElts.list, taskApp.view.taskTemplate); 
    taskApp.taskList = taskList;
    taskApp.filter.updateCategories(taskList);
    taskApp.view.render(taskApp.allCategories, htmlElts.categoryList, taskApp.view.categoryTemplate);
  }

})(this, taskApp.view.html);