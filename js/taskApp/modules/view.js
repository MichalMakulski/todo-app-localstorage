(function(public){
  
  public.taskApp = public.taskApp || {};

  // html string to render
  var htmlStr = '';

  // html elements:
  var html = {  
    form: document.querySelector('#task-form'),
    name: document.querySelector('#task-name'),
    desc: document.querySelector('#task-desc'),
    deadline: document.querySelector('#task-deadline'),
    categories: document.querySelector('#task-categories'),
    btn: document.querySelector('#add-task'),
    list: document.querySelector('.task-list'),
    categoryList: document.querySelector('.category-list')
  }

  // function to render arrays into html container
  function render(arr, container, template) {   
    htmlStr = '';
    arr.forEach(template);
    container.innerHTML = htmlStr;   
  }

  // html template for task
  function taskTemplate(val, idx) {
    if(val === undefined){
      return;
    }else{
      var state = val.completed ? ' task-done' : '';
      var deadlineClass = deadlineCheck(val);
      htmlStr += '<li class="task-item' + state + '"' + 'data-taskid=' + idx + '>' + 
              '<span class="deadline' + deadlineClass + '"></span>' +
              '<h4 class="task-title">' + val.name + '</h4>' +
              '<p class="task-desc">' + val.description + '</p>' + 
              '<span class="delete-btn">DELETE</span><span class="edit-btn">EDIT</span>' +
              '</li>';
    }
  }

  // html template for categories
  function categoryTemplate(val, idx) {
    htmlStr += '<li class="category-item">' + val + '</li>';
  }
  
  // function that compares task deadline with today date and returns appropriate class for the warning badge:
  function deadlineCheck(obj){
    var classStr = '';
    var millisecondsPerDay = 24 * 60 * 60 * 1000;
    var today = new Date();
    var diffInDays = (new Date(obj.deadline) - today) / millisecondsPerDay;
    if(diffInDays <= 5) {
      classStr = ' danger';
    }else if(diffInDays > 5 && diffInDays <= 15){
      classStr = ' warning';
    }else {
      classStr = '';
    }
    return classStr;
  }
  
  taskApp.view = {
    html: html,
    render: render,
    taskTemplate: taskTemplate,
    categoryTemplate: categoryTemplate,
  }
  
})(this);