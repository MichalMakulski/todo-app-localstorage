(function(public){
  
  public.taskApp = public.taskApp || {};

  // arrays used in filtering tasks:
  var selectedCategories = [];
  var filteredTasks = [];
  

  // gather all categories from all tasks and put them in allCategories array with unique values
  function updateCategories(arr){
    var a = [];
    arr.forEach(function(val){
       a = a.concat(val.categories);
    });
    taskApp.allCategories = a.filter(function(val, idx, self) {
      return idx === self.indexOf(val);
    });
  }
  
  // filter function for tasks based on selected categories
  function filterTasks(ev, arr){
    var target = ev.target;
    var id;
    if(target.classList.contains('category-item')){
      if(!target.inArray){
        selectedCategories.push(target.textContent);
        target.inArray = true;
      }else {
       selectedCategories = selectedCategories.filter(function(val) {
          return val !== target.textContent;
        });
        target.inArray = false;
      }
      filteredTasks = arr.map(function(val, idx){
        for(var i = 0; i < selectedCategories.length; i++){
          if(val.categories.indexOf(selectedCategories[i]) !== -1){
            id = idx;
            return val; 
          }else{
            delete filteredTasks[idx];
          }
        } 
      });
      if(selectedCategories.length > 0){
        taskApp.view.render(filteredTasks, taskApp.htmlTaskList, taskApp.view.taskTemplate);
      }else {
        taskApp.view.render(taskApp.taskList, taskApp.htmlTaskList, taskApp.view.taskTemplate); 
      }   
      target.classList.toggle('cat-selected');
    }
  }
  
  taskApp.filter = {
    updateCategories: updateCategories,
    filterTasks: filterTasks
  }
  
})(this);