(function(public){ 

  // main task array and html string to render:
  var taskList;
  var html = '';
  
  // arrays used in filtering tasks:
  var allCategories = [];
  var selectedCategories = [];
  var filteredTasks = [];
  
  // fallback to document.cookie
  function saveData(key, value){
    if(window.localStorage){
      localStorage.setItem(key, value);
    }else{
      document.cookie = key + '=' + value;
    }
  }
  
  function getData(key){
    if(window.localStorage){
      taskList = localStorage[key] ? JSON.parse(localStorage[key]) : [];
    }else{
        var cookies = document.cookie.replace(/\s/g, '').split(';');
        var o = {};
        function assignToObj(cookie){
          var c = cookie.replace(/=/, ';').split(';');
          o[c[0]] = c[1];
        }
        cookies.forEach(assignToObj);
        document.cookies = o;
        taskList =  document.cookies[key] ? JSON.parse(document.cookies[key]) : [];
    }
  }
  
  // html elements:
  var form = document.querySelector('#task-form');
  var name = document.querySelector('#task-name');
  var desc = document.querySelector('#task-desc');
  var deadline = document.querySelector('#task-deadline');
  var categories = document.querySelector('#task-categories');
  var btn = document.querySelector('#add-task');
  var list = document.querySelector('.task-list');
  var categoryList = document.querySelector('.category-list');
  
  // function to render arrays into html container
  function render(arr, container, template) {   
    html = '';
    arr.forEach(template);
    container.innerHTML = html;   
  }
  
  // html template for task
  function taskTemplate(val, idx) {
    var state = val.completed ? ' task-done' : '';
    var deadlineClass = deadlineCheck(val);
    html += '<li class="task-item' + state + '"' + 'data-taskid=' + idx + '>' + 
            '<span class="deadline' + deadlineClass + '"></span>' +
            '<h4 class="task-title">' + val.name + '</h4>' +
            '<p class="task-desc">' + val.description + '</p>' + 
            '<span class="delete-btn">DELETE</span><span class="edit-btn">EDIT</span>' +
            '</li>';
  }
  
  // html template for categories
  function categoryTemplate(val, idx) {
    html += '<li class="category-item">' + val + '</li>';
  }
  
  // gather all categories from all tasks and put them in allCategories array with unique values
  function updateCategories(){
    var a = [];
    taskList.forEach(function(val){
       a = a.concat(val.categories);
    });
    allCategories = a.filter(function(val, idx, self) {
      return idx === self.indexOf(val);
    });
  }
  
  // filter function for tasks based on selected categories
  function filterTasks(ev){
    ev.preventDefault();
    ev.stopPropagation();
    var target = ev.target;
    var id;
    if(target.classList.contains('category-item')){
      if(!target.inArray){
        selectedCategories.push(target.textContent);
        target.inArray = true;
      }else {
       selectedCategories = selectedCategories.filter(function(val) {
          return val !== target.textContent;
        });
        target.inArray = false;
      }
      filteredTasks = taskList.filter(function(val, idx){
        for(var i = 0; i < selectedCategories.length; i++){
          if(val.categories.indexOf(selectedCategories[i]) !== -1){
            id = idx;
            return true;
            
          }
        }
        return false;
      });
      if(filteredTasks.length > 0){
        render(filteredTasks, list, taskTemplate);
      }else {
        render(taskList, list, taskTemplate); 
      }   
      target.classList.toggle('cat-selected');
    }
  }
  
  // function that compares task deadline with today date and returns appropriate class for the warning badge:
  // add INTERVAL
  function deadlineCheck(obj){
    var classStr = '';
    var millisecondsPerDay = 24 * 60 * 60 * 1000;
    var today = new Date();
    var diffInDays = (new Date(obj.deadline) - today) / millisecondsPerDay;
    if(diffInDays <= 5) {
      classStr = ' danger';
    }else if(diffInDays > 5 && diffInDays <= 15){
      classStr = ' warning';
    }else {
      classStr = '';
    }
    return classStr;
  }
  
  // editTask combines 3 scenarios - clearing, editing and deleting task:
  function editTask(ev){
    ev.preventDefault();
    ev.stopPropagation();
    var target = ev.target;
    if(target.classList.contains('task-item')){
      var completed = taskList[target.dataset.taskid].completed;
      target.classList.toggle('task-done');
      taskList[target.dataset.taskid].completed = !completed;
      saveData('tasks', JSON.stringify(taskList));
    }else if(target.classList.contains('delete-btn')) {
      if(confirm('Are you sure you want to delete this task?')){
        var taskEl = target.parentElement.dataset.taskid;
        taskList.splice(parseInt(taskEl), 1);
        saveData('tasks', JSON.stringify(taskList));
        render(taskList, list, taskTemplate);
        
        // test
        updateCategories();
        render(allCategories, categoryList, categoryTemplate);
      }
    }else if(target.classList.contains('edit-btn')) {
      var taskEl = target.parentElement.dataset.taskid;    
      
      // populate form inputs with task data:
      name.value = taskList[taskEl].name;
      desc.value = taskList[taskEl].description;
      deadline.value = taskList[taskEl].deadline;
      categories.value = taskList[taskEl].categories.join(',');
      
      // test
      updateCategories();
      render(allCategories, categoryList, categoryTemplate);
      
      // modify form submit button:
      btn.value = 'SAVE TASK';
      btn.style.backgroundColor = '#BA68C8';
      
      // modify form properties:
      form.mode = 'update';
      form.editedTask = taskList[taskEl];
    } 
  }
  
  // adds or updates the task (depends on form.mode value):
  function addTask(ev) {
    ev.preventDefault();
    ev.stopPropagation(); 
    var taskName = name.value;
    var taskDesc = desc.value;
    var deadlineDate = deadline.value;
    var taskCategories = categories.value.replace(/\s/g, '').split(',');
    if (!taskName) {
      alert('Name your task...');
    }else {
      if(form.mode === 'update'){
        form.editedTask.name = taskName;
        form.editedTask.description = taskDesc;
        form.editedTask.deadline = deadlineDate;
        form.editedTask.categories = taskCategories;
        form.mode = 'create';
        btn.value = 'ADD TASK';
        btn.style.backgroundColor = '';
      }else {
        var newTask = {
          name: taskName,
          description: taskDesc,
          deadline: deadlineDate,
          categories: taskCategories
        }
        taskList.push(newTask);
      }  
    }  
    saveData('tasks', JSON.stringify(taskList));
    render(taskList, list, taskTemplate);
    
    // test
    updateCategories();
    render(allCategories, categoryList, categoryTemplate);
  }
  
  function bindEvents(){
    form.addEventListener('submit', addTask, false);
    list.addEventListener('click', editTask, false);
    categoryList.addEventListener('click', filterTasks, false);
  }

  public.init = function(){
    bindEvents();
    getData('tasks');
    render(taskList, list, taskTemplate); 
    
    // test
    updateCategories();
    render(allCategories, categoryList, categoryTemplate);
  }
})(this.taskApp = {});

document.addEventListener('DOMContentLoaded', taskApp.init)