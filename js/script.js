;(function() {

  function createScript(url){
    var fScript = document.querySelector('script');
    var p = new Promise(function(resolve, reject){
        var script  = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;
        script.onload = function(){
          resolve("asdasd");
        }
        fScript.parentElement.insertBefore(script, fScript);   
    });
    return p;
  }

  createScript('js/taskApp/modules/view.js').
    then(function(){
      Promise.all([
          createScript('js/taskApp/taskApp.js'),
          createScript('js/taskApp/modules/filter.js')
        ]).then(function(data){
          taskApp.init();
        });
    });
  
})();